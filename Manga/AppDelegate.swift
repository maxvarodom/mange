import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import GoogleSignIn
import RxSwift
import BackgroundTasks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
		
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
		FirebaseApp.configure()
		GIDSignIn.sharedInstance().clientID = "490430774833-so14f3ugrv7bpth9i49mv6pe7ggqicg7.apps.googleusercontent.com"
		UNUserNotificationCenter.current().delegate = self
		let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
		UNUserNotificationCenter.current().requestAuthorization(
			options: authOptions,
			completionHandler: {_, _ in })
		Messaging.messaging().delegate = self
		application.registerForRemoteNotifications()
						
		BGTaskScheduler.shared.register(forTaskWithIdentifier: "com.backgroundTasks", using: nil) { (bgTask) in
			
			let disposeBag = DisposeBag()
			
			MangaSeedService()
				.callService(request: ManageSeedRequestModel(nogzip: "1", page: "0"))
				.subscribeOn(globalScheduler)
				.observeOn(globalScheduler)
				.filterNilManageSeedArray()
				.insertFavoriteToModel()
				.observeOn(MainScheduler.asyncInstance)
				.bind { (result) in
					for mange in result.reversed() {
						if mange.favorite {
							self.sendNotification(title: "เรื่องใหม่มาแย้วน้าง้าบ", body: mange.name ?? "", urlString: mange.img)
						}
					}
			}
			.disposed(by: disposeBag)
			self.scheduleAppRefresh()
		}
		return true
	}

	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url)
        return googleDidHandle
    }

    func application(_ application: UIApplication,open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url)
        return googleDidHandle
    }
	
	func applicationDidEnterBackground(_ application: UIApplication) {
		scheduleAppRefresh()
	}
	
	func scheduleAppRefresh() {
		let request = BGAppRefreshTaskRequest(identifier: "com.backgroundTasks")
		request.earliestBeginDate = Date(timeIntervalSinceNow: 60 * 2)
		do {
			try BGTaskScheduler.shared.submit(request)
		} catch {
			print("Could not schedule app refresh: \(error)")
		}
	}
	
	func sendNotification(title: String, body: String, urlString: String?) {
		let notificationContent = UNMutableNotificationContent()
		notificationContent.title = title
		notificationContent.body = body
		notificationContent.badge = NSNumber(value: 3)
		
		if let unrawURLString = urlString, let url = URL(string: unrawURLString) {
			let urlImageFormDisk = SaveImageToFile().read(fileName: url.path.replacingOccurrences(of: "/", with: ""))
			if let attachment = try? UNNotificationAttachment(identifier: UUID().uuidString,
															  url: URL(fileURLWithPath: urlImageFormDisk),
															  options: nil) {
				notificationContent.attachments = [attachment]
			}
		}
		
		let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5,
														repeats: false)
		let request = UNNotificationRequest(identifier: UUID().uuidString,
											content: notificationContent,
											trigger: trigger)
		
		UNUserNotificationCenter.current().add(request) { (error) in
			if let error = error {
				print("Notification Error: ", error)
			}
		}
	}
}

extension AppDelegate: UNUserNotificationCenterDelegate {
	
	func application(application: UIApplication,
					 didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
		Messaging.messaging().apnsToken = deviceToken as Data
	}
}

extension AppDelegate: MessagingDelegate {
	
	func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
		print("Firebase registration token: \(fcmToken)")
	}
}
