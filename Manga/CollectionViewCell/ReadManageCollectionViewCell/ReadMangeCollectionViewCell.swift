import UIKit
import AlamofireImage

class ReadMangeCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var manageImageView: UIImageView!
	@IBOutlet weak var imageViewWidthConstraintLayout: NSLayoutConstraint!
	@IBOutlet weak var imageViewHeightConstraintLayout: NSLayoutConstraint!

	override func awakeFromNib() {
		super.awakeFromNib()
		imageViewWidthConstraintLayout.constant = UIScreen.main.bounds.width
	}
	
	override func prepareForReuse() {
		super.prepareForReuse()
		manageImageView.image = nil
	}
}
