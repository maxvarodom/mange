import UIKit
import RxSwift

class ManageSeedNextChapterCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var nextChapterButton: UIButton!
	
	var disposeBag = DisposeBag()
	
	override func prepareForReuse() {
		super.prepareForReuse()
		disposeBag = DisposeBag()
	}
}
