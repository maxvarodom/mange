import UIKit
import AlamofireImage

class ManageSeedCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var mangeImageView: UIImageView!
	@IBOutlet weak var favoriteImageView: UIImageView!
	@IBOutlet weak var nameManageLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
	@IBOutlet weak var backgroudStatusView: UIView!
	
	override func prepareForReuse() {
		super.prepareForReuse()
		mangeImageView.image = nil
	}
}

extension ManageSeedCollectionViewCell {
	
	func loadingImageForPresentFormDiskOrOutsite(fromAnime: StoryAnimeXMLModel) {
		mangeImageView.loadImageFromStore(urlString: fromAnime.urlImage)
	}
	
	func setFieldPresentCell(fromAnime: StoryAnimeXMLModel) {
		statusLabel.text = ""
		nameManageLabel.text = fromAnime.titleStory
		favoriteImageView.image = fromAnime.favorite ? #imageLiteral(resourceName: "star_full") : #imageLiteral(resourceName: "star")
		statusLabel.text = ""
	}
}

extension ManageSeedCollectionViewCell {
	
	func loadingImageForPresentFormDiskOrOutsite( _ element: MapManageSeedResponseModel) {
		mangeImageView.loadImageFromStore(urlString: element.img)
	}
	
	func setFieldPresentCell( _ element: MapManageSeedResponseModel) {
		favoriteImageView.image = element.favorite ? #imageLiteral(resourceName: "star_full") : #imageLiteral(resourceName: "star")
		nameManageLabel.text = element.name
		statusLabel.text = ""
	}
	
	func feedTodayCell( _ element: MapManageSeedResponseModel) {
		let hot = element.hot == "1"
		let new = element.new == "1"
		
		if hot {
			statusLabel.text = "HOT"
			statusLabel.textColor = UIColor.red
		}
		
		if new {
			statusLabel.text = "NEW"
			statusLabel.textColor = UIColor.orange
		}
		
		backgroudStatusView.isHidden = (new == hot)
	}
}
