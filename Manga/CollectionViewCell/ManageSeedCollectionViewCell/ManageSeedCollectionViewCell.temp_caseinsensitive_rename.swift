import UIKit

class ManageSeedCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var manageImageView: UIImageView!
	@IBOutlet weak var nameManageLabel: UILabel!
}
