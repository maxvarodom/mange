import UIKit

extension UIImageView {
	
	func loadImageFromStore(urlString: String?) {
		guard
			let unrawURLString = urlString,
			let urlImage = URL(string: unrawURLString) else {
				return
		}
		
		let urlImageFormDisk = SaveImageToFile().read(fileName: urlImage.path.replacingOccurrences(of: "/", with: ""))
		if let imageContentsOfFile = UIImage(contentsOfFile: urlImageFormDisk) {
			image = imageContentsOfFile
		} else {
			UIImageView.saveOfFile(by: urlImage) { [weak self] _ in
				self?.loadImageFromStore(urlString: urlString)
			}
		}
	}
	
	static func loadImageToPath(urlString: String?) -> UIImage? {
		guard
			let unrawURLString = urlString,
			let urlImage = URL(string: unrawURLString) else {
				return nil
		}
		
		let urlImageFormDisk = SaveImageToFile().read(fileName: urlImage.path.replacingOccurrences(of: "/", with: ""))
		return UIImage(contentsOfFile: urlImageFormDisk)
	}
		
	static func saveOfFile(by url: URL, callback: @escaping (URL) -> Void) {
		DispatchQueue.global().async {
			let partImage = url.path.replacingOccurrences(of: "/", with: "")
			if UIImageView.loadImageToPath(urlString: partImage) == nil {
				Store.saveImages(by: url)
				guard let data = try? Data(contentsOf: url), let image = UIImage(data: data) else {
					DispatchQueue.main.async {
						callback(url)
					}
					return
				}
				SaveImageToFile().write(name: partImage, image: image)
				DispatchQueue.main.async {
					callback(url)
				}
			} else {
				DispatchQueue.main.async {
					callback(url)
				}
			}
		}
	}
}

