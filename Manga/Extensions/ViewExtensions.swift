import UIKit

extension UIView {
	
	@IBInspectable final internal var setCornerBackground: Bool {
		get {
			return false
		}
		set {
			guard newValue else { return }
			self.layer.cornerRadius = self.bounds.height / 4
			self.layer.masksToBounds = true
		}
	}
	
	@IBInspectable final internal var cornerRadius: CGFloat {
		get {
			return 0.0
		}
		set {
			self.layer.cornerRadius = newValue
		}
	}
	
	@IBInspectable final internal var masksToBounds: Bool {
		get {
			return false
		}
		set {
			self.layer.masksToBounds = newValue
		}
	}
	
	@IBInspectable var shadowRadius: CGFloat {
		get {
			return 0.0
		}
		set {
			self.layer.shadowRadius = newValue
		}
	}
	
	@IBInspectable var shadowOpacity: Float {
		get {
			return 0.0
		}
		set {
			self.layer.shadowOpacity = newValue
		}
	}
	
	@IBInspectable var shadowOffset: CGSize {
		get {
			return CGSize(width: 0.0, height: 0.0)
		}
		set {
			self.layer.shadowOffset = newValue
		}
	}
	
	@IBInspectable var borderWidth: CGFloat {
		get {
			return 0.0
		}
		set {
			self.layer.borderWidth = newValue
		}
	}
}
