import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: UITableView {
	
	public func items<S: Sequence, Cell: UITableViewCell, O : ObservableType>
		(cellType: Cell.Type = Cell.self)
		-> (_ source: O)
		-> (_ configureCell: @escaping (Int, S.Iterator.Element, Cell) -> Void)
		-> Disposable
		where O.Element == S {
			let buildToString = String(describing: Cell.self)
			return self.items(cellIdentifier: buildToString, cellType: Cell.self)
	}
}

extension Reactive where Base: UICollectionView {

	public func items<S: Sequence, Cell: UICollectionViewCell, O : ObservableType>
		(cellType: Cell.Type = Cell.self)
		-> (_ source: O)
		-> (_ configureCell: @escaping (Int, S.Iterator.Element, Cell) -> Void)
		-> Disposable
		where O.Element == S {
			let buildToString = String(describing: Cell.self)
			return self.items(cellIdentifier: buildToString, cellType: Cell.self)
	}
}
