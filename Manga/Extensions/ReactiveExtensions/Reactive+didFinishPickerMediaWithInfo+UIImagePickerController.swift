#if os(iOS)
#if !RX_NO_MODULE
import RxSwift
import RxCocoa
#endif
import UIKit

extension Reactive where Base: UIImagePickerController {
	
	public var didFinishPickingMediaWithInfo: Observable<[UIImagePickerController.InfoKey : Any]> {
		return delegate
			.methodInvoked(#selector(UIImagePickerControllerDelegate.imagePickerController(_:didFinishPickingMediaWithInfo:)))
			.map({ (a) in
				return try castOrThrow(Dictionary<UIImagePickerController.InfoKey, Any>.self, a[1])
			})
	}
	
	public var didCancel: Observable<()> {
		return delegate
			.methodInvoked(#selector(UIImagePickerControllerDelegate.imagePickerControllerDidCancel(_:)))
			.map {_ in () }
	}
}

#endif

fileprivate func castOrThrow<T>(_ resultType: T.Type, _ object: Any) throws -> T {
	guard let returnValue = object as? T else {
		throw RxCocoaError.castingError(object: object, targetType: resultType)
	}
	return returnValue
}
