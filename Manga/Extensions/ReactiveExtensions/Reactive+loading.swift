import UIKit
import RxCocoa
import RxSwift

fileprivate var loadingBlockScreenView = LoadingBlockScreen(frame: UIScreen.main.bounds)

extension ObservableType {
	
	func startPresentLoading(_ message: String = "") -> RxSwift.Observable<Self.Element> {
		return self
			.observeOn(MainScheduler.asyncInstance)
			.`do`(onNext: { [weak loadingBlockScreenView] (_) in
				guard let `loadingBlockScreenView` = loadingBlockScreenView else { return }
				UIApplication.shared.keyWindow?.addSubview(loadingBlockScreenView)
				loadingBlockScreenView.text?.text = message
				loadingBlockScreenView.show()
			})
	}
	
	func endPresentLoading() -> RxSwift.Observable<Self.Element> {
		return self
			.observeOn(MainScheduler.asyncInstance)
			.`do`(onNext: { [weak loadingBlockScreenView] (_) in
				loadingBlockScreenView?.hide()
				},onError: { [weak loadingBlockScreenView] (_) in
					loadingBlockScreenView?.hide()
			})
	}
}
