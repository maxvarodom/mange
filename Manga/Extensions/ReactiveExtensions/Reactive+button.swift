import RxCocoa
import RxSwift
import UIKit

extension Reactive where Base : UIButton {
	
	public var tapObject: Observable<Reactive<Base>> {
		return tap.map { self }
	}
}
