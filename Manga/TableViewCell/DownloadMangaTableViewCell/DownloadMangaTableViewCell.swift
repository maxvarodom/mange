import UIKit
import RxSwift

class DownloadMangaTableViewCell: UITableViewCell {
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!
	@IBOutlet weak var downloadButton: UIButton!
	
	var disposeBag = DisposeBag()
	
	override func prepareForReuse() {
		super.prepareForReuse()
		disposeBag = DisposeBag()
		downloadButton.setTitle("Download", for: UIControl.State.normal)
	}
}
