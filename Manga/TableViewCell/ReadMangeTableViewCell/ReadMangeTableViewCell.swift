import UIKit

@IBDesignable class ReadMangeTableViewCell: UITableViewCell {
	
	@IBOutlet weak var mangeImageView: UIImageView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	override func prepareForReuse() {
		super.prepareForReuse()
		updateConstraints()
		updateFocusIfNeeded()
		updateConstraintsIfNeeded()
	}
}
