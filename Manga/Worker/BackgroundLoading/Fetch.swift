import UIKit
import RxSwift
import RxCocoa

enum ImageError: Error {
	case canLoading
}

class Fetch {
	
	init() { }
	
	var disposeBag = DisposeBag()
	var eventLoadingCounting = BehaviorRelay<(Int, Int)>.init(value: (0, 0))
	var loadingImage: ((URL) -> Void)?
	
	func loadReadMange(id: String?, chapter: String?, alertController: UIAlertController? = nil) -> Observable<[MapMangeImageResponseModel]> {
		var counting = Int()
		return ManageImageService()
			.callService(request: ManageImageRequestModel(id: id, chapter: chapter, nogzip: "1"))
			.do(onNext: { (response) in
				Store.saveMangaReaderJSON(id: id, chapter: chapter, by: response)
			})
			.map({ (manageImageResponseModel) -> [MapMangeImageResponseModel] in
				return manageImageResponseModel.result ?? []
			})
			.flatMap({ [weak self] (w) in
				return Observable<[MapMangeImageResponseModel]>.create { [weak self] (oberver) -> Disposable in
					for element in w {
						guard
							let urlString = element.img,
							let url = URL(string: urlString) else {
								continue
						}
						
						UIImageView.saveOfFile(by: url) { (url) in
							counting += 1
							
							DispatchQueue.main.async {
								self?.eventLoadingCounting.accept((counting, w.count))
								alertController?.title = "\(counting + 1) / \(w.count)"
							}
							
							if counting == (w.count - 1) {
								if alertController == nil {
									oberver.onNext(w)
								}
								DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25) {
									alertController?.title = "สำเร็จจำนวนหน้า \(w.count)"
								}
								
								DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
									alertController?.dismiss(animated: true, completion: {
										oberver.onNext(w)
									})
								}
							}
						}
					}
					
					return Disposables.create()
				}
			})
	}
	
	func loadImages(urlStrings: [String?], alertController: UIAlertController?, startIndex: Int = 0, counting: Int = 0, usingBe: [[String?]], maxLoad: Int) {
		var countingT = counting
		var loadCounting = Int()

		for urlString in usingBe[startIndex] {
			guard let unurlString = urlString, let url = URL(string: unurlString) else {
				countingT += 1
				loadCounting += 1
				continue
			}
			
			UIImageView.saveOfFile(by: url) { (url) in
				countingT += 1
				loadCounting += 1
				DispatchQueue.main.async {
					alertController?.title = "กำลังโหลดรูป"
					alertController?.message = "\(countingT) / \(urlStrings.count)"
				}
								
				if countingT >= (urlStrings.count) {
					DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25) {
						alertController?.title = "สำเร็จจำนวนหน้า \(urlStrings.count)"
						DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
							alertController?.dismiss(animated: true)
						}
					}
					return
				}
				
				if maxLoad == loadCounting {
					self.loadImages(urlStrings: urlStrings, alertController: alertController, startIndex: startIndex + 1, counting: countingT, usingBe: usingBe, maxLoad: maxLoad)
				}
			}
		}
	}
}

extension Array {
	func chunked(into size: Int) -> [[Element]] {
		return stride(from: 0, to: count, by: size).map {
			Array(self[$0 ..< Swift.min($0 + size, count)])
		}
	}
}
