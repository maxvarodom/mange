import UIKit
import RxSwift
import RxCocoa

enum ImageError: Error {
	case canLoading
}

class Fetch {
	
	var disposeBag = DisposeBag()
	
	func loadReadMange(id: String?, chapter: String?)  {
		ManageImageService()
			.callService(request: ManageImageRequestModel(id: id, chapter: chapter, nogzip: "1"))
			.map({ (manageImageResponseModel) -> [MapMangeImageResponseModel] in
				return manageImageResponseModel.result ?? []
			})
			.map({ [weak self] (w) -> [MapMangeImageResponseModel] in
				return try w.map({ [weak self] (m) -> MapMangeImageResponseModel in
					guard let urlString = m.img else {
						throw ImageError.canLoading
					}
					guard let url = URL(string: urlString) else {
						throw ImageError.canLoading
					}
					self?.saveOfFile(by: url)
					return m
				})
			})
			.subscribe()
			.disposed(by: disposeBag)
	}
	
	func saveOfFile(by url: URL) {
		let saveImageToFile = SaveImageToFile()
		let partImage = url.path.replacingOccurrences(of: "/", with: "")
		guard saveImageToFile.hasFile(name: partImage) else { return }
		Store.saveImages(by: url)
		guard let data = try? Data(contentsOf: url) else { return }
		guard let image = UIImage(data: data) else { return }
		saveImageToFile.write(name: partImage, image: image)
	}
}
