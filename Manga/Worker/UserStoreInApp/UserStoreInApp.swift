import Foundation

enum UserStoreInAppError: Error {
	case notHasUserLogin
}

class UserStoreInApp {
	
	static let flagLogin: String = "AKSMdoiasd092imOINoINGODINosig933"
	static let flagGoogleDrive: String = "ASLKmfoi3ngno42ignonrogknsdkgl"

	static func setUserLogin(with id: String) {
		UserDefaults.standard.set(id, forKey: flagLogin)
	}
	
	static func getUserLogin() throws -> String {
		guard let userId = UserDefaults.standard.object(forKey: flagLogin) as? String else {
			throw UserStoreInAppError.notHasUserLogin
		}
		return userId
	}
	
	static func setUserIdGoogleDrive(with id: String) {
		UserDefaults.standard.set(id, forKey: flagGoogleDrive)
	}
	
	static func getUserGoogleDrive() throws -> String {
		guard let userId = UserDefaults.standard.object(forKey: flagGoogleDrive) as? String else {
			throw UserStoreInAppError.notHasUserLogin
		}
		return userId
	}
}
