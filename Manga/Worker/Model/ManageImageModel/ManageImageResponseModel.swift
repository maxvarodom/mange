import ObjectMapper

class ManageImageResponseModel: BaseResponseModel {
	
	var result : [MapMangeImageResponseModel]?

	override init() {
		super.init()
	}
	
	required init?(map: Map) {
		super.init(map: map)
	}
	
	override func mapping(map: Map) {
		super.mapping(map: map)
		result <- map["result"]
	}
}

struct MapMangeImageResponseModel : Mappable {
	
	var isLast: Bool = false
	var img : String?
	
	init() { }
	
	init?(map: Map) { }
	
	mutating func mapping(map: Map) {
		img <- map["img"]
	}
}
