import ObjectMapper

class ManageImageRequestModel: BaseRequestModel {
	
	var id: String?
	var chapter: String?
	var nogzip: String?
	
	init(id: String?, chapter: String?, nogzip: String?) {
		super.init()
		self.id = id
		self.chapter = chapter
		self.nogzip = nogzip
	}
	
	required init?(map: Map) {
		super.init()
	}
	
	override func mapping(map: Map) {
		super.mapping(map: map)
	}
}
