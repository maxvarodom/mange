import ObjectMapper

class BaseResponseModel: Mappable {
	
	var status: Int?
	
	init() { }
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		status <- map["status"]
	}
}
