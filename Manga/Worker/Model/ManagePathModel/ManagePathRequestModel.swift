import ObjectMapper

class ManagePathRequestModel: BaseRequestModel {
	
	var id: String?
	var nogzip: String?
	
	init(id: String?, nogzip: String?) {
		super.init()
		self.id = id
		self.nogzip = nogzip
	}
	
	required init?(map: Map) {
		super.init()
	}
	
	override func mapping(map: Map) {
		super.mapping(map: map)
	}
}
