import ObjectMapper
import RxSwift
import RxCocoa
import Differentiator

class ManagePathResponseModel: BaseResponseModel {
	
	var result : [MapMangePathResponseModel]?
	var desc : Desc?
	var related : [Related]?
	
	override init() {
		super.init()
	}
	
	required init?(map: Map) {
		super.init(map: map)
	}
	
	override func mapping(map: Map) {
		super.mapping(map: map)
		result <- map["result"]
		desc <- map["desc"]
		related <- map["related"]
	}
}

struct Related : Mappable {
	var id : String?
	var release : String?
	var name : String?
	var name_thai : String?
	var end : String?
	var new : String?
	var img : String?
	
	init?(map: Map) { }
	
	mutating func mapping(map: Map) {
		id <- map["id"]
		release <- map["release"]
		name <- map["name"]
		name_thai <- map["name_thai"]
		end <- map["end"]
		new <- map["new"]
		img <- map["img"]
	}
}

struct MapMangePathResponseModel : Mappable {
	var part : String?
	var name : String?
	var isnew : String?
	var isSee: Bool = false
	var uuid: String = UUID().uuidString
	var downloading: String = ""
	
	init() { }
	
	init?(map: Map) { }
	
	mutating func mapping(map: Map) {
		part <- map["part"]
		name <- map["name"]
		isnew <- map["isnew"]
	}
}

struct Desc : Mappable, Codable {
	var author : String?
	var artist : String?
	var genres : String?
	var status : String?
	var detail : String?
	var img : String?
	var name : String?
	var id : String?
	var rating : String?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		
		author <- map["author"]
		artist <- map["artist"]
		genres <- map["genres"]
		status <- map["status"]
		detail <- map["detail"]
		img <- map["img"]
		name <- map["name"]
		id <- map["id"]
		rating <- map["rating"]
	}
	
}

extension MapMangePathResponseModel: IdentifiableType {
	
	typealias Identity = String
	
	var identity: String {
		return uuid
	}
}

extension MapMangePathResponseModel: Equatable {
	static func == (lhs: MapMangePathResponseModel, rhs: MapMangePathResponseModel) -> Bool {
		return lhs.part == rhs.part 
	}
}
