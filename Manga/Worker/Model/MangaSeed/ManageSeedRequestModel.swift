import ObjectMapper

class ManageSeedRequestModel: BaseRequestModel {
	
	var nogzip: String?
	var page: String?
	
	init(nogzip: String, page: String) {
		super.init()
		self.nogzip = nogzip
		self.page = page
	}
	
	required init?(map: Map) {
		super.init()
	}
	
	override func mapping(map: Map) {
		super.mapping(map: map)
		nogzip <- map["nogzip"]
		page <- map["page"]
	}
}
