import ObjectMapper
import RxDataSources
import Differentiator

class ManageSeedResponseModel: BaseResponseModel {
	
	var result: [MapManageSeedResponseModel]?
	
	override init() {
		super.init()
	}
	
	required init?(map: Map) {
		super.init()
	}
	
	override func mapping(map: Map) {
		super.mapping(map: map)
		result <- map["result"]
	}
}

class MapManageSeedResponseModel : Mappable {
	var id : String?
	var release : String?
	var time : Int?
	var view : String?
	var name : String?
	var end : String?
	var new : String?
	var hot : String?
	var update : String?
	var vip : String?
	var status : String?
	var img : String?
	var uuid = UUID().uuidString
	var favorite: Bool = false
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		id <- map["id"]
		release <- map["release"]
		time <- map["time"]
		view <- map["view"]
		name <- map["name"]
		end <- map["end"]
		new <- map["new"]
		hot <- map["hot"]
		update <- map["update"]
		vip <- map["vip"]
		status <- map["status"]
		img <- map["img"]
		uuid <- map["uuid"]
		favorite <- map["favorite"]
	}
}

extension MapManageSeedResponseModel: IdentifiableType {
	
	typealias Identity = String
	
	var identity: String {
		return uuid
	}
}

extension MapManageSeedResponseModel: Equatable {
	static func == (lhs: MapManageSeedResponseModel, rhs: MapManageSeedResponseModel) -> Bool {
		return lhs.uuid == rhs.uuid
	}
}
