import FirebaseDatabase
import CodableFirebase
import Firebase
import RxSwift

class FirebaseStore {
	
	private static let flagHistory = "historys"
	private static let flagFavorites = "favorites"

}

extension FirebaseStore {
	
	static func setHistoryToFirebase(with child: String, and models: [MangeChapterHistoryModel]) {
		var ref: DatabaseReference!
		ref = Database.database().reference()
		ref.child(FirebaseStore.flagHistory).child(child).setValue(models.toJSON())
	}
		
	static func getSaveHistoryFromFirebase(firebaseChild: String) -> Observable<[MangeChapterHistoryModel]> {
		return Observable<Any>.create { (observer) -> Disposable in
			Database
				.database()
				.reference()
				.child(FirebaseStore.flagHistory)
				.child(firebaseChild)
				.observeSingleEvent(of: DataEventType.value, with: { (dataSnapshot) in
					guard let values = dataSnapshot.value else {
						observer.onError(StoreError.favoriteNotField)
						return
					}
					observer.onNext(values)
				}, withCancel: { (error) in
					observer.onNext(error)
				})
			return Disposables.create()
			}
			.map({ (values) -> [MangeChapterHistoryModel] in
				let model = try? FirebaseDecoder().decode([MangeChapterHistoryModel].self, from: values)
				return model ?? []
			})
	}
}

extension FirebaseStore {
	
	static func setFavoriteToFirebase(with child: String, and models: [MangeFavoriteModel]) {
		var ref: DatabaseReference!
		ref = Database.database().reference()
		ref.child(FirebaseStore.flagFavorites).child(child).setValue(models.toJSON())
	}
	
	static func getFavoritesFromFirebase(firebaseChild: String) -> Observable<[MangeFavoriteModel]> {
		return Observable<Any>.create { (observer) -> Disposable in
			Database
				.database()
				.reference()
				.child(FirebaseStore.flagFavorites)
				.child(firebaseChild)
				.observeSingleEvent(of: DataEventType.value, with: { (dataSnapshot) in
					guard let values = dataSnapshot.value else {
						observer.onError(StoreError.favoriteNotField)
						return
					}
					observer.onNext(values)
				}, withCancel: { (error) in
					observer.onNext(error)
				})
			return Disposables.create()
			}
			.map({ (values) -> [MangeFavoriteModel] in
				let model = try? FirebaseDecoder().decode([MangeFavoriteModel].self, from: values)
				return model ?? []
			})
	}

}
