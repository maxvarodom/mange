import UIKit

class SaveImageToFile {
	
	let fileManager = FileManager.default
	
	func hasFile(name: String) -> Bool {
		let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
		let fileURL = documentsURL.appendingPathComponent(name)
		return !fileManager.fileExists(atPath: fileURL.path)
	}
		
	func write(name: String, image: UIImage) {
		do {
			let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
			let fileURL = documentsURL.appendingPathComponent(name)
			if let pngImageData = image.pngData() {
				try pngImageData.write(to: fileURL, options: .noFileProtection)
			}
		} catch { }
	}
	
	func read(fileName: String) -> String {
		let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
		let filePath = documentsURL.appendingPathComponent("\(fileName)").path
		return filePath
	}
	
	func removeAll(callback: @escaping ((Int, Int, Error?) -> Void)) {
		DispatchQueue.global().async {
			
			var counting = 0
			var fullCount = 0
			
			let documentsURL = self.fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
			
			do {
				let fileURLs = try self.fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
				
				if fileURLs.count == 0 {
					DispatchQueue.main.async {
						callback(0, 0, nil)
					}
					return
				}
				
				fullCount = fileURLs.count
				for path in fileURLs.map({ t in t.path }) {
					try self.fileManager.removeItem(atPath: path)
					counting += 1
					DispatchQueue.main.async {
						callback(counting, fullCount, nil)
					}
				}
			} catch {
				print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
				DispatchQueue.main.async {
					callback(counting, fullCount, error)
				}
			}
		}
	}
}
