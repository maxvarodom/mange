import ObjectMapper
import UIKit

enum StoreError: Error {
	case favoriteNotField
}

class Store {
	
	private static let flagSaveHistory = "IMDOINo9e3hjfin0fjJ(T#*U)$^TJ)INienf"
	private static let flagSaveFavorite = "ALSKNDOKASMdoi3t9f39tgnsjNODNSFmaodfnaoj93n"
	
}

extension Store {
	
	@discardableResult
	static func setSaveHistory(with flag: MangeChapterHistoryModel) -> [MangeChapterHistoryModel] {
		var history = Store.getSaveHistory()
		history.append(flag)
		guard let toJSON = history.toJSONString() else {
			return history
		}
		UserDefaults.standard.set(toJSON, forKey: flagSaveHistory)
		return history
	}
	
	static func getSaveHistory() -> [MangeChapterHistoryModel] {
		guard let history = UserDefaults.standard.object(forKey: flagSaveHistory) as? String else {
			return []
		}
		let bindHistory = Mapper<MangeChapterHistoryModel>().mapArray(JSONString: history)
		return bindHistory ?? []
	}
		
	@discardableResult
	static func replaceHistoryToUserDefaults(with models: [MangeChapterHistoryModel]) -> [MangeChapterHistoryModel] {
		guard let toJSON = models.toJSONString() else {
			return models
		}
		UserDefaults.standard.set(toJSON, forKey: flagSaveHistory)
		return models
	}
}

extension Store {
	
	@discardableResult
	static func setFavorite(with flag: MangeFavoriteModel) -> [MangeFavoriteModel] {
		var history = Store.getFavorite()
		history.append(flag)
		guard let toJSON = history.toJSONString() else {
			return []
		}
		UserDefaults.standard.set(toJSON, forKey: flagSaveFavorite)
		return history
	}
	
	@discardableResult
	static func replaceFavoriteToUserDefaults(with models: [MangeFavoriteModel]) -> [MangeFavoriteModel] {
		guard let toJSON = models.toJSONString() else {
			return models
		}
		UserDefaults.standard.set(toJSON, forKey: flagSaveFavorite)
		return models
	}
	
	static func getFavorite() -> [MangeFavoriteModel] {
		guard let favorite = UserDefaults.standard.object(forKey: flagSaveFavorite) as? String else {
			return []
		}
		let bindFavorite = Mapper<MangeFavoriteModel>().mapArray(JSONString: favorite)
		return bindFavorite ?? []
	}
	
	@discardableResult
	static func removeFavorite(with id: String) -> [MangeFavoriteModel] {
		var favorite = Store.getFavorite()
		favorite.removeAll { (mangeFavoriteModel) -> Bool in
			return id == mangeFavoriteModel.id
		}
		guard let toJSON = favorite.toJSONString() else {
			return favorite
		}
		UserDefaults.standard.set(toJSON, forKey: flagSaveFavorite)
		return favorite
	}
}

class MangeFavoriteModel: Mappable, Codable {
	
	var id: String?
	var favorite: Bool = false
	var desc: Desc?
	
	init(id: String?, favorite: Bool) {
		self.id = id
		self.favorite = favorite
	}
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		id <- map["id"]
		favorite <- map["favorite"]
		desc <- map["desc"]
	}
}

class MangeChapterHistoryModel: Mappable, Codable {
	
	var read : Bool?
	var id : String?
	var capter: String?
	
	init(read: Bool?, id: String?, capter: String?) {
		self.read = read
		self.id = id
		self.capter = capter
	}
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		read <- map["read"]
		id <- map["id"]
		capter <- map["capter"]
	}
}

fileprivate let documentDirectory = FileManager.init().urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first!
fileprivate let productArchiveURL = documentDirectory.appendingPathComponent("imagesDocumentDirectory")
fileprivate let saveMangaSeedJSONArchiveURL = documentDirectory.appendingPathComponent("saveMangaSeedJSONDocumentDirectory")

extension Store {
	
	@discardableResult
	internal static func saveImages(by images: URL) -> Bool {
		var copy = getImages()
		copy.append(images)
		return NSKeyedArchiver.archiveRootObject(copy, toFile: productArchiveURL.path)
	}
	
	@discardableResult
	internal static func getImages() -> [URL] {
		let directory = NSKeyedUnarchiver.unarchiveObject(withFile: productArchiveURL.path) as? [URL]
		return directory ?? []
	}
	
	@discardableResult
	static func saveMangaSeedJSON(by any: [MapManageSeedResponseModel]) -> Bool {
		return NSKeyedArchiver.archiveRootObject(any.toJSON(), toFile: saveMangaSeedJSONArchiveURL.path)
	}
	
	@discardableResult
	static func getMangaSeedJSON() -> [MapManageSeedResponseModel] {
		guard let directory = NSKeyedUnarchiver.unarchiveObject(withFile: saveMangaSeedJSONArchiveURL.path) as? [[String: Any]] else { return [] }
		let convect = Mapper<MapManageSeedResponseModel>.init().mapArray(JSONArray: directory)
		return convect
	}
	
	@discardableResult
	static func saveChapterJSON(by any: ManagePathResponseModel, with id: String?) -> Bool {
		let archiveURL = documentDirectory.appendingPathComponent("chapter-offline/\(id ?? "")")
		return NSKeyedArchiver.archiveRootObject(any.toJSON(), toFile: archiveURL.path)
	}
	
	@discardableResult
	static func getChapterJSON(id: String?) -> ManagePathResponseModel {
		let archiveURL = documentDirectory.appendingPathComponent("chapter-offline/\(id ?? "")")
		let directory = NSKeyedUnarchiver.unarchiveObject(withFile: archiveURL.path) as? [String: Any]
		let convect = Mapper<ManagePathResponseModel>.init().map(JSON: directory ?? [:])
		return convect ?? ManagePathResponseModel()
	}
	
	@discardableResult
	static func saveMangaReaderJSON(id: String?, chapter: String?, by any: ManageImageResponseModel) -> Bool {
		let archiveURL = documentDirectory.appendingPathComponent("reader-offline/\(id ?? "") \(chapter ?? "")")
		return NSKeyedArchiver.archiveRootObject(any.toJSON(), toFile: archiveURL.path)
	}
	
	@discardableResult
	static func getMangaReaderJSON(id: String?, chapter: String?) -> ManageImageResponseModel? {
		let archiveURL = documentDirectory.appendingPathComponent("reader-offline/\(id ?? "") \(chapter ?? "")")
		let directory = NSKeyedUnarchiver.unarchiveObject(withFile: archiveURL.path) as? [String: Any]
		let convect = Mapper<ManageImageResponseModel>().map(JSON: directory ?? [:])
		return convect
	}
	
	static func read() {
		
		// Get the document directory url
		let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
		
		do {
			let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
			let jpgFiles = directoryContents.filter{ $0.pathExtension == "jpg" }

			
			for jpgURL in directoryContents {
				
			}
			
//			let mp3FileNames = mp3Files.map{ $0.deletingPathExtension().lastPathComponent }
//			print("mp3 list:", mp3FileNames)
			
		} catch {
			print(error)
		}
	}
}
