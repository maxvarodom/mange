import RxSwift

class ManageChapterService: BaseService<ManagePathRequestModel, ManagePathResponseModel> {
	
	override func callService(request: ManagePathRequestModel) -> Observable<ManagePathResponseModel> {
		return buildRequest(url: Router.getcartoonpart(parameters: request.toJSON(), element: request), requestModel: request)
	}
}
