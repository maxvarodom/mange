import RxSwift
import RxCocoa

class ManageImageService: BaseService<ManageImageRequestModel, ManageImageResponseModel> {
	
	override func callService(request: ManageImageRequestModel) -> Observable<ManageImageResponseModel> {
		if let getFromStore = Store.getMangaReaderJSON(id: request.id, chapter: request.chapter),
			let increaseAbility = try? increaseAbility(response: getFromStore) {
			return Observable.just(increaseAbility)
		}

		return super.buildRequest(url: Router.getcartoondetail(parameters: request.toJSON(), element: request), requestModel: request)
	}
	
	override func increaseAbility(response: ManageImageResponseModel) throws -> ManageImageResponseModel {
		guard response.result != nil else {
			throw ServiceError.noResult
		}

		let copyResponse = response
		var result = MapMangeImageResponseModel()
		result.isLast = true
		copyResponse.result?.append(result)
		return copyResponse
	}
}
