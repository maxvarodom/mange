import RxSwift
import ObjectMapper

class SearchResultMangeSeedRequestModel: BaseRequestModel {
	
	var query: String?
	
	init(query: String) {
		super.init()
		self.query = query
	}
	
	required init?(map: Map) {
		super.init()
	}
	
	override func mapping(map: Map) {
		super.mapping(map: map)
		query <- map["query"]
	}
}


class SearchResultMangeSeed: BaseService<SearchResultMangeSeedRequestModel, ManageSeedResponseModel> {
	
	override func callService(request: SearchResultMangeSeedRequestModel) -> Observable<ManageSeedResponseModel> {
		return buildRequest(url: Router.search(parameters: request.toJSON(), query: request.query ?? ""), requestModel: request)
	}
}
