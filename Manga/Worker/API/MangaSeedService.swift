import RxSwift

class MangaSeedService: BaseService<ManageSeedRequestModel, ManageSeedResponseModel> {
	
	override func callService(request: ManageSeedRequestModel) -> Observable<ManageSeedResponseModel> {
		return buildRequest(url: Router.mangaSeed(parameters: request.toJSON(), element: request), requestModel: request)
	}
}
