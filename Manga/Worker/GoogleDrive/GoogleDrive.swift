import GoogleSignIn

class GoogleDrive {
	
	static let share = GoogleDrive()
	
	var googleUser: GIDGoogleUser?
	
	init() {
		googleUser = GIDSignIn.sharedInstance()?.currentUser
	}
}
