enum ServiceError: Error {
	case response(String)
	case mapperError
	case cannonMapperFailNotJSON
	case successFail
	case successNil
	case noInternetConnection
	case hostNameCouldNotBeFound
	case linkNotFound
	case serverDown
	case notHasKeyUrlFormRequestModel
	case notHost
	case notMethod
	case noNextChapter
	case cannotCountingChapter
	case noResult
    case newResult
}
