import Foundation
import RxSwift
import Dispatch
import DispatchIntrospection

let globalScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
let concurrentScheduler = ConcurrentDispatchQueueScheduler(qos: DispatchQoS.background)
let serialScheduler = SerialDispatchQueueScheduler(qos: DispatchQoS.utility)
let operationScheduler = OperationQueueScheduler(operationQueue: OperationQueue())
