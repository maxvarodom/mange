import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class Manager {
	
	private let sessionManager: Session
	
	let serverTrustPolicies: [String: ServerTrustEvaluating] = [
		"api.manga-seed.com": PinnedCertificatesTrustEvaluator()
	]
	
	public init() {
		sessionManager = Session()
	}
		
	func sessionURL(urlRequest: URLRequestConvertible) -> Observable<(DataResponse<Any, AFError>)> {
		return sessionManager.rx.request(urlRequest: urlRequest)
			.responseJSON()
			.subscribeOn(globalScheduler)
			.observeOn(globalScheduler)
	}
}
