import ObjectMapper

class JsonMapper {

    static let shareInstance = JsonMapper()

    func mapper<Response>( _ response: Any?) throws -> Response where Response : BaseMappable{
        guard let json = Mapper<Response>().map(JSONObject: response) else {
                throw ServiceError.mapperError
        }
        return json
    }

}
