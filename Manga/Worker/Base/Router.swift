import Alamofire
import Foundation

enum Router : URLRequestConvertible {
	
	static var version = "11.0"
	static var nogzip = "1"
	
	case mangaSeed(parameters: Parameters, element: ManageSeedRequestModel)
	case getcartoonpart(parameters: Parameters, element: ManagePathRequestModel)
	case getcartoondetail(parameters: Parameters, element: ManageImageRequestModel)
	case search(parameters: Parameters, query: String)

	var method: HTTPMethod {
		switch self {
		case .mangaSeed: return .get
		case .getcartoonpart: return .get
		case .getcartoondetail: return .get
		case .search: return .get
		}
	}
	
	var path: String {
		switch self {
		case .mangaSeed:
			return "mangaseed/cartoon.php/androidapiv\(Router.version)/getcartoon"
		case .getcartoonpart(parameters: _, element: let element):
			let id = element.id ?? ""
			return "mangaseed/cartoon.php/androidapiv\(Router.version)/getcartoonpart/\(id)?nogzip=\(Router.nogzip)"
		case .getcartoondetail(parameters: _, element: let element):
			let id = element.id ?? ""
			let chapter = element.chapter ?? ""
			return "mangaseed/cartoon.php/androidapiv\(Router.version)/getcartoondetail/\(chapter)/\(id)"
		case .search:
			return "mangaseed/androidapiv\(Router.version)/search"
		}
	}
	
	var parameters: Parameters {
		switch self {
		case .mangaSeed(parameters: let parameter, _):
			return parameter
		case .getcartoonpart(parameters: let parameter, element: _):
			return parameter
		case .getcartoondetail(parameters: let parameter, element: _):
			return parameter
		case .search(parameters: let parameter, query: _):
			return parameter
		}
	}
}

extension Router {
	
	func asURLRequest() throws -> URLRequest {
		let url: URL! = URL(string: "https://api.manga-seed.com/" + path)
		var urlRequest = URLRequest(url: url)
		urlRequest.httpMethod = method.rawValue
		urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
		urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
		return urlRequest
	}
}
