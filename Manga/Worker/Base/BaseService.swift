import RxSwift
import RxCocoa
import Alamofire

internal class BaseService<Request: BaseRequestModel, Response: BaseResponseModel> {
	
	internal init() {}
	
	internal func validateRequest(request: Request) throws { }
	
	internal func validateResponse(response: Response) throws -> Response {
		guard (response.status ?? 0) == 200 else {
			throw ServiceError.response("Error 200.")
		}
		return response
	}
	
	internal func increaseAbility(response: Response) throws -> Response {
		return response
	}
	
	internal func callService( request: Request) -> Observable<Response> {
		preconditionFailure("Call service must be overridden")
	}
	
	internal func buildRequest(url: URLRequestConvertible, requestModel: Request) -> Observable<Response>  {
		return Maybe
			.just((url,requestModel))
			.subscribeOn(globalScheduler)
			.observeOn(globalScheduler)
			.map({ (urlRequestConvertible, request) -> (URLRequestConvertible) in
				try self.validateRequest(request: request)
				return (url)
			})
			.asObservable()
			.flatMap({ (urlRequestConvertible) -> Observable<DataResponse<Any, AFError>> in
				return Manager().sessionURL(urlRequest: urlRequestConvertible)
			})
			.map({ dataResponse -> Response in
				return try JsonMapper.shareInstance.mapper(dataResponse.value) })
			.map { try self.validateResponse(response: $0) }
			.map { try self.increaseAbility(response: $0) }
			.catchError { VerifyErrorType.shareInstance.verifyErrorType($0) }
	}
}
