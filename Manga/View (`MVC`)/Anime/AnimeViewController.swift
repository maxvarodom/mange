import UIKit
import SwiftSoup

struct StoryAnimeXMLModel {
	let titleStory: String
	let urlImage: String
	let urlStory: String
	var favorite: Bool
}

class AnimeViewController: BaseViewController {
	
	@IBOutlet weak var collectionView: UICollectionView!
	
	var storyAnimes: [StoryAnimeXMLModel]? = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Anime"
		collectionView.delegate = self
		collectionView.dataSource = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		storyAnimes?.removeAll()
		requestAnimeSugoiHomePage()
	}
	
	private func requestAnimeSugoiHomePage() {
		
		let request = NSMutableURLRequest(url: URL(string: "https://www.anime-sugoi.com")!,
										  cachePolicy: .useProtocolCachePolicy,
										  timeoutInterval: 10.0)
		request.httpMethod = "GET"
		let session = URLSession.shared
		let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
			if !(error != nil) {
				self?.htmlToJSON(data: data!)
				
				DispatchQueue.main.async {
					self?.collectionView.reloadData()
				}
			}
		})
		
		dataTask.resume()
	}
	
	private func htmlToJSON(data: Data) {
		if let html = String(data: data, encoding: .utf8) {
			do {
				let els: Elements = try SwiftSoup.parse(html).select("div").select("a")
				for (link) in els.array() {
					let linkHref: String = try link.attr("href")
					let linkImg: String = try link.children().attr("src")
					let linkText: String = try link.attr("title")
					guard linkText != "" else { continue }
					let favorite = Store.getFavorite().contains(where: { $0.id == linkHref && $0.favorite })
					storyAnimes?.append(StoryAnimeXMLModel(titleStory: linkText, urlImage: linkImg, urlStory: linkHref, favorite: favorite))
				}
				
				storyAnimes = storyAnimes?.filterDuplicates(includeElement: { $0.titleStory == $1.titleStory })
				
			} catch {
				handleAlertViews(error: error)
			}
		}
	}
}

extension AnimeViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return storyAnimes?.count ?? 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ManageSeedCollectionViewCell", for: indexPath) as! ManageSeedCollectionViewCell
		guard let element = storyAnimes?[indexPath.row] else { return cell }
		cell.setFieldPresentCell(fromAnime: element)
		cell.loadingImageForPresentFormDiskOrOutsite(fromAnime: element)
		cell.setFieldPresentCell(fromAnime: element)
		return cell
	}
}

extension AnimeViewController: UICollectionViewDelegate {
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let element = storyAnimes?[indexPath.row] else { return }
		router?.pushToChapterAnimeViewController(storyAnime: element)
	}
}
