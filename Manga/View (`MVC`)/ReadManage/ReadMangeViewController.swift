import UIKit
import RxCocoa
import RxSwift
import Alamofire
import AlamofireImage

struct ReadMangeModel {
	var prepareCapterId: String?
	var prepareId: String?
}

enum ViewMode {
	case table
	case collection
}

class ReadMangeViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var backToChapterButton: UIButton!
	@IBOutlet weak var backgroundChapterView: UIView!
	@IBOutlet weak var backgroundViewModeView: UIView!
	@IBOutlet weak var backgroundNextChapterView: UIView!
	@IBOutlet weak var viewModelButton: UIButton!
	@IBOutlet weak var lnextChapterButton: UIButton!
	var nextChapterButton: UIButton!
	
	var stockReadMange: [MapMangeImageResponseModel] = []
	var nextStockReadMange: [MapMangeImageResponseModel] = []
	var readMangeModel: ReadMangeModel?
	var prepareMangeAllChapter: ManagePathResponseModel?
	var isModeAutoDirectionMode = true
	var viewMode = ViewMode.table
	
	var resizingImage = [UIImage]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		let nextChapter = setupNavigationBarAddFavoriteButton()
		navigationItem.rightBarButtonItems = [nextChapter]
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
			self.collectionView.delegate = self
			self.collectionView.dataSource = self
		}
		collectionViewRegisterNib()
		setupEventSaveHistory()
		loadingNextChapter()
		eventPresentTitleViewController()
		setupEventNextChapter()
		setupEventViewModeSelected(by: viewModelButton)
		autoSelectionScrollDirectionMode()
		eventPopToChapterActionButton()
				
		let isHidden = navigationController?.isNavigationBarHidden ?? true
		backToChapterButton.isHidden = isHidden
		backgroundChapterView.isHidden = isHidden
		backgroundViewModeView.isHidden = isHidden
		backgroundNextChapterView.isHidden = isHidden
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return stockReadMange.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let element = stockReadMange[indexPath.row]
		if element.isLast {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ManageSeedNextChapterCollectionViewCell", for: indexPath) as! ManageSeedNextChapterCollectionViewCell
			cell.nextChapterButton.rx.tap
				.bind(onNext: { [weak self] _ in self?.nextChapter() })
				.disposed(by: cell.disposeBag)
			return cell
		} else {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReadMangeCollectionViewCell", for: indexPath) as! ReadMangeCollectionViewCell
			let image = UIImageView.loadImageToPath(urlString: element.img)
			let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
			if flowLayout?.scrollDirection == .vertical {
				cell.manageImageView.contentMode = .scaleAspectFill
				cell.manageImageView.image = image
			} else {
				cell.manageImageView.contentMode = .scaleAspectFit
				cell.manageImageView.image = image
			}
			return cell
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
		let element = stockReadMange[indexPath.row]
				
		if flowLayout?.scrollDirection == .vertical {
			guard let pathImage = element.img else { return .zero }
			guard let pathToURL = try? pathImage.asURL() else { return .zero }
			let partfile = pathToURL.path.replacingOccurrences(of: "/", with: "")
			guard let image = UIImage(contentsOfFile: SaveImageToFile().read(fileName: partfile)) else {
				return CGSize(width: UIScreen.main.bounds.width, height: 100)
			}
			let scale = UIScreen.main.bounds.width / image.size.width
			let newHeight = detectSizeImage(element) * scale
			
			if UIDevice.current.userInterfaceIdiom == .pad {
				return CGSize(width: image.size.width, height: image.size.height)
			}
			
			return CGSize(width: UIScreen.main.bounds.width, height: newHeight)
		} else {
			return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		var isHidden = navigationController?.isNavigationBarHidden ?? true
		isHidden.toggle()
		backToChapterButton.isHidden = isHidden
		backgroundChapterView.isHidden = isHidden
		backgroundViewModeView.isHidden = isHidden
		backgroundNextChapterView.isHidden = isHidden
		navigationController?.setNavigationBarHidden(isHidden, animated: true)
	}
}

extension ReadMangeViewController: UIScrollViewDelegate {

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if (scrollView.contentOffset.y >= (scrollView.contentSize.height + scrollView.contentInset.bottom - scrollView.bounds.height) + 180) {
			nextChapter()
		}

		if (scrollView.contentOffset.y <= 0) {

		}

		if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)) {

		}
	}
}

extension ReadMangeViewController {
	
	func detectSizeImage(_ element: MapMangeImageResponseModel) -> CGFloat {
		guard let pathImage = element.img else { return CGFloat() }
		guard let pathToURL = try? pathImage.asURL() else { return CGFloat() }
		let partfile = pathToURL.path.replacingOccurrences(of: "/", with: "")
		let image = UIImage(contentsOfFile: SaveImageToFile().read(fileName: partfile))
		let height = image?.size.height ?? CGFloat()
		return height
	}
	
	func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
		let scale = newWidth / image.size.width
		let newHeight = image.size.height * scale
		UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
		image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return newImage!
	}
}

extension ReadMangeViewController {
	
	func autoSelectionScrollDirectionMode() {
		
		guard isModeAutoDirectionMode else {
			selectedViewMode(self.viewMode)
			return
		}
		
		let countInStock = stockReadMange.count
		var countingOverSize = Int()
		for element in stockReadMange {
			let height = detectSizeImage(element)
			if height > 2000.0 {
				countingOverSize += 1
			}
		}
		let abs = (countInStock / 2)
		if countingOverSize > abs {
			selectedViewMode(.table)
		} else {
			selectedViewMode(.collection)
		}
		
		isModeAutoDirectionMode = false
	}
	
	func selectedViewMode(_ viewMode: ViewMode) {
		self.viewMode = viewMode
		guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
		flowLayout.minimumLineSpacing = 0
		flowLayout.minimumInteritemSpacing = 0
		
		if viewMode == .table {
			flowLayout.scrollDirection = .vertical
			collectionView.isPagingEnabled = false
		}
		
		if viewMode == .collection {
			flowLayout.scrollDirection = .horizontal
			collectionView.isPagingEnabled = true
		}
		
		collectionView.setCollectionViewLayout(flowLayout, animated: true)
	}
}

extension ReadMangeViewController {
	
	func collectionViewRegisterNib() {
		collectionView.register(UINib(nibName: "ReadMangeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ReadMangeCollectionViewCell")
		collectionView.register(UINib(nibName: "ManageSeedNextChapterCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ManageSeedNextChapterCollectionViewCell")
	}
}

extension ReadMangeViewController {

	func loadingNextChapter() {
		nextChapterButton.isEnabled = false
		nextChapterButton.alpha = 0.6
		var chapterId = readMangeModel?.prepareCapterId
		let prepareId = readMangeModel?.prepareId
		try? plusCountingChapter(chapterId: &chapterId)
		
		let fetch = Fetch()
		
		fetch
			.loadReadMange(id: prepareId, chapter: chapterId)
			.observeOn(MainScheduler.asyncInstance)
			.bind(onNext: { [weak self] result in
				self?.nextChapterButton.isEnabled = true
				self?.nextChapterButton.alpha = 1.0
				self?.nextChapterButton.setTitle("Next", for: UIControl.State.normal)
				self?.nextStockReadMange = result
			})
			.disposed(by: disposeBag)

		fetch
			.eventLoadingCounting
			.observeOn(MainScheduler.asyncInstance)
			.bind { [weak self] (seed, week) in
				self?.nextChapterButton.setTitle("\(seed)/\(week)", for: UIControl.State.normal)
				
				guard let `self` = self, seed == week else { return }
				
				if self.isNextChapter() {
					self.nextChapterButton.setTitle("Next", for: UIControl.State.normal)
				} else {
					self.nextChapterButton.setTitle("-", for: UIControl.State.normal)
				}

			}
			.disposed(by: disposeBag)
	}
}

extension ReadMangeViewController {

	func eventPresentTitleViewController() {
		let chapter = prepareMangeAllChapter?.result?.first(where: { [weak self] (mapMangePathResponseModel) -> Bool in
			return mapMangePathResponseModel.part == self?.readMangeModel?.prepareCapterId
		})
		title = chapter?.name
	}
}

extension ReadMangeViewController {

	func setupEventSaveHistory() {
		let id = readMangeModel?.prepareId
		let capter = readMangeModel?.prepareCapterId
		let createHistory = MangeChapterHistoryModel(read: true, id: id, capter: capter)
		let retureVulesHistory = Store.setSaveHistory(with: createHistory)
		guard let userId = try? UserStoreInApp.getUserLogin() else {
			return
		}
		FirebaseStore.setHistoryToFirebase(with: userId, and: retureVulesHistory)
	}
}

extension ReadMangeViewController {

	func setupNavigationBarAddFavoriteButton() -> UIBarButtonItem {
		nextChapterButton = UIButton(type: .custom)
		nextChapterButton.frame = CGRect(x: 0, y: 0, width: 50, height: 31)
		let barButton = UIBarButtonItem(customView: nextChapterButton)
		barButton.width = 31
		return barButton
	}

	func setupEventNextChapter() {
		nextChapterButton.rx.tap
			.bind(onNext: { [weak self] _ in self?.nextChapter() })
			.disposed(by: disposeBag)
		
		lnextChapterButton.rx.tap
			.bind(onNext: { [weak self] _ in self?.nextChapter() })
			.disposed(by: disposeBag)
	}
	
	func nextChapter() {
		var chapterId = readMangeModel?.prepareCapterId
		let prepareId = readMangeModel?.prepareId
		try? plusCountingChapter(chapterId: &chapterId)
		let buildReadMangeModel = ReadMangeModel(prepareCapterId: chapterId, prepareId: prepareId)
		let allChapter = prepareMangeAllChapter
		let storyboard = UIStoryboard(name: "ReadMange", bundle: nil)
		if let createViewController = storyboard.instantiateViewController(withIdentifier: "ReadMangeViewController") as? ReadMangeViewController {
			createViewController.readMangeModel = buildReadMangeModel
			createViewController.prepareMangeAllChapter = allChapter
			createViewController.stockReadMange = nextStockReadMange
			createViewController.isModeAutoDirectionMode = self.isModeAutoDirectionMode
			createViewController.viewMode = self.viewMode
			guard let navigationController = self.navigationController else {
				self.present(createViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(createViewController, animated: true)
		}
	}
	
	func plusCountingChapter(chapterId: inout String?) throws {
		guard let index = getIndexFromAllChapter(chapterId) else { throw ServiceError.noNextChapter }
		guard isNextChapter() else { throw ServiceError.noNextChapter }
		guard let nextChapterId = prepareMangeAllChapter?.result?[index - 1].part else { throw ServiceError.noNextChapter }
		chapterId = nextChapterId
	}

	func getIndexFromAllChapter(_ chapterId: String?) -> Int? {
		let index = prepareMangeAllChapter?.result?.firstIndex(where: { (result) -> Bool in
			return result.part == chapterId
		})
		return index
	}

	func isNextChapter() -> Bool {
		let chapterId = readMangeModel?.prepareCapterId
		guard let index = getIndexFromAllChapter(chapterId) else { return false }
		return index > 0
	}
}

extension ReadMangeViewController {
	
	func setupEventViewModeSelected(by button: UIButton) {
		button.rx.tap
			.bind { [weak self] (_) in
				let isViewMode = (button.tag) % 2 == 1
				button.tag += 1
				if isViewMode {
					self?.selectedViewMode(.collection)
				} else {
					self?.selectedViewMode(.table)
				}
			}
			.disposed(by: disposeBag)
	}
	
	func eventPopToChapterActionButton() {
		backToChapterButton.rx.tap
			.bind { [weak self] _ in
				guard let navigationController = self?.navigationController else {
					self?.dismiss(animated: true)
					return
				}
				navigationController.viewControllers.forEach{ controller in
					guard controller is ChapterViewController else {
						return
					}
					navigationController.popToViewController(controller, animated: true)
				}
			}
			.disposed(by: disposeBag)
	}
}
