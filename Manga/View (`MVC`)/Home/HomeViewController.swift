import UIKit

class HomeViewController: BaseViewController {
		
	@IBAction func goToMangaPageButton(sender: UIButton) {
		router?.pushToMangaViewController()
	}
	
	@IBAction func goToAnimePageButton(sender: UIButton) {
		router?.pushToAnimeViewController()
	}
}
