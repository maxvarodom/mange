import UIKit
import WebKit
import SwiftSoup

class AnimePlayerViewController: BaseViewController {
	
	@IBOutlet weak var webView: WKWebView!
	
	var prepareChapter: ChapterAnimeXMLModel?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		title = prepareChapter?.title
		
		let id = prepareChapter?.chapterLink
		let capter = prepareChapter?.title
		let createHistory = MangeChapterHistoryModel(read: true, id: id, capter: capter)
		let retureVulesHistory = Store.setSaveHistory(with: createHistory)
		guard let userId = try? UserStoreInApp.getUserLogin() else { return }
		FirebaseStore.setHistoryToFirebase(with: userId, and: retureVulesHistory)
		
		guard
			let chapterLink = try? prepareChapter?.chapterLink.asURL(),
			let data = try? Data(contentsOf: chapterLink),
			let html = String(data: data, encoding: .utf8) else {
				navigationController?.popViewController(animated: true)
				return
		}
		
		do {
			let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
			let matches = detector.matches(in: html, options: [], range: NSRange(location: 0, length: html.utf16.count))
			
			for match in matches {
				guard let range = Range(match.range, in: html) else { continue }
				let url = html[range]
				if url.contains("https://www.anime-sugoi.com/player/") {
					guard
						let url = try? url.description.asURL(),
						let data = try? Data(contentsOf: url),
						let html = String(data: data, encoding: .utf8)
						else {
							break
					}
					
					let webview = UIWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
					webview.loadHTMLString(html, baseURL: nil)
					self.view.addSubview(webview)
					break
				}
			}
			
		} catch {
			handleAlertViews(error: error)
		}
	}
}
