import UIKit
import RxSwift
import RxCocoa

class ChapterViewController: BaseViewController {
	
	@IBOutlet weak var tableView: UITableView!
	var addFavoriteButton: UIButton!
	var saveAllMangaButton: UIButton!
	
	var id: String?
	var favorite = false
	var saveAllMangePathResponseModel = ManagePathResponseModel()
	var reloadMangeChapter: BehaviorRelay<[MapMangePathResponseModel]> = BehaviorRelay<[MapMangePathResponseModel]>(value: [])
	var didSelectionTableViewHistoryRow: IndexPath?
	var storeHistory = [MangeChapterHistoryModel]()
	static var isFavReload = false
	static var id: String?
	static var favorite = false
			
	override func viewDidLoad() {
		super.viewDidLoad()
		reloadMangeChapter.accept(Store.getChapterJSON(id: id).result ?? [])
		reloadManageCapture()
		setupTableViewSelectManageCapter()
		setupTableViewPresentManagePath()
		ChapterViewController.isFavReload = true
		ChapterViewController.id = self.id
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(false, animated: animated)
		reloadManageCapture()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		setupAnimationSeeToRowHistory(animated: animated)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		navigationController?.setNavigationBarHidden(false, animated: animated)
	}
	
	func reloadManageCapture() {
		
		storeHistory = Store.getSaveHistory()
			.filter({ [weak self] (mangeChapterHistoryModel) -> Bool in
				return mangeChapterHistoryModel.id == self?.id
			})
			.reduce(into: [MangeChapterHistoryModel]()) { (uniqueElements, element) in
				if !uniqueElements.contains(where: { (mangeChapterHistoryModel) -> Bool in
					return mangeChapterHistoryModel.capter == element.capter
				}) {
					uniqueElements.append(element)
				}
		}
				
		ManageChapterService()
			.callService(request: ManagePathRequestModel(id: id, nogzip: "1"))
			.observeOn(MainScheduler.asyncInstance)
			.do(onNext: { [weak self] (response) in
				self?.saveAllMangePathResponseModel = response
				self?.title = response.desc?.name
				
				let getFavorite = Store.getFavorite().first { (element) -> Bool in
					return element.id == response.desc?.id
				}
				
				self?.favorite = getFavorite?.favorite ?? false
				ChapterViewController.favorite = getFavorite?.favorite ?? false
				self?.setupNavigationBarAddFavoriteButton()
				self?.setupNavigationBarSaveAllMangaButtonBar()
				self?.setupEventAddFavoriteBarButton()
				self?.setupEventSaveAllMangaBarButton()
			})
			.observeOn(concurrentScheduler)
			.map({ (managePathResponseModel) -> [MapMangePathResponseModel] in
				return managePathResponseModel.result ?? []
			})
			.bind(to: reloadMangeChapter)
			.disposed(by: disposeBag)
	}
	
	func setupTableViewPresentManagePath() {
		reloadMangeChapter
			.observeOn(MainScheduler.asyncInstance)
			.bind(to: tableView.rx.items(cellIdentifier: "ChapterTableViewCell", cellType: UITableViewCell.self))
			{ [weak self] row, element, cell in
				cell.textLabel?.text = "Chapter"
				cell.detailTextLabel?.text = element.name
				
				let hasHistory = self?.storeHistory.contains(where: { [weak self] (manageSeeHistoryModel) -> Bool in
					return element.part == manageSeeHistoryModel.capter && self?.id == manageSeeHistoryModel.id
				}) ?? false
				
				cell.imageView?.image =  hasHistory ? UIImage(systemName: "book.fill") : UIImage(systemName: "book")
			}
			.disposed(by: disposeBag)
	}
	
	func setupTableViewSelectManageCapter() {
		tableView.rx.itemSelected
			.bind { [weak self] indexPath in
				self?.didSelectionTableViewHistoryRow = indexPath
			}
			.disposed(by: disposeBag)
		
		tableView.rx.modelSelected(MapMangePathResponseModel.self)
			.startPresentLoading()
			.flatMap({ [weak self] (element) -> Observable<(MapMangePathResponseModel,[MapMangeImageResponseModel])> in
				
				let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
				let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
				loadingIndicator.hidesWhenStopped = true
				loadingIndicator.style = UIActivityIndicatorView.Style.gray
				loadingIndicator.startAnimating();
				alert.view.addSubview(loadingIndicator)
				self?.present(alert, animated: true, completion: nil)

				let capterId = element.part
				let id = self?.id
				let observer = Fetch().loadReadMange(id: id, chapter: capterId, alertController: alert)
				return Observable.combineLatest(Observable.just(element), observer)
			})
			.subscribeOn(MainScheduler.asyncInstance)
			.observeOn(MainScheduler.asyncInstance)
			.endPresentLoading()
			.bind { [weak self] (mapMangePathResponseModel, result) in
				let capterId = mapMangePathResponseModel.part
				let id = self?.id
				let createReadMangeModel = ReadMangeModel(prepareCapterId: capterId, prepareId: id)
				let storyboard = UIStoryboard(name: "ReadMange", bundle: nil)
				if let createViewController = storyboard.instantiateViewController(withIdentifier: "ReadMangeViewController") as? ReadMangeViewController {
					createViewController.readMangeModel = createReadMangeModel
					createViewController.prepareMangeAllChapter = self?.saveAllMangePathResponseModel
					createViewController.stockReadMange = result
					guard let navigationController = self?.navigationController else {
						self?.present(createViewController, animated: true, completion: nil)
						return
					}
					navigationController.pushViewController(createViewController, animated: true)
				}
			}
			.disposed(by: disposeBag)
	}
	
	func setupNavigationBarAddFavoriteButton() {
		addFavoriteButton = UIButton(type: .custom)
		addFavoriteButton.setImage((favorite) ? #imageLiteral(resourceName: "star_full") : #imageLiteral(resourceName: "star") , for: .normal)
		addFavoriteButton.frame = CGRect(x: 0, y: 0, width: 31, height: 31)
		let barButton = UIBarButtonItem(customView: addFavoriteButton)
		barButton.width = 31
		self.navigationItem.rightBarButtonItems = [barButton]
	}
	
	func setupNavigationBarSaveAllMangaButtonBar() {
		saveAllMangaButton = UIButton(type: .custom)
		saveAllMangaButton.frame = CGRect(x: 0, y: 0, width: 31, height: 31)
		saveAllMangaButton.setTitle("Save All", for: .normal)
		saveAllMangaButton.setTitleColor(.black, for: .normal)
		let barButton = UIBarButtonItem(customView: saveAllMangaButton)
		barButton.width = 31
		self.navigationItem.rightBarButtonItems?.append(barButton)
	}
	
	func setupEventAddFavoriteBarButton() {
		addFavoriteButton.rx.tap
			.bind { [weak self] (_) in
				if (self?.favorite ?? false) {
					guard let id = self?.id else { return }
					let retureValueFavoriteInfo = Store.removeFavorite(with: id)
					self?.setFavoriteToFirebase(with: retureValueFavoriteInfo)
					self?.favorite = false
					ChapterViewController.favorite = false
				} else {
					guard let id = self?.id else { return }
					let createMangeFavorite = MangeFavoriteModel(id: id, favorite: true)
					let retureValueFavoriteInfo = Store.setFavorite(with: createMangeFavorite)
					self?.setFavoriteToFirebase(with: retureValueFavoriteInfo)
					self?.favorite = true
					ChapterViewController.favorite = true
				}
				let isFavorite = (self?.favorite ?? false)
				self?.addFavoriteButton.setImage(isFavorite ? #imageLiteral(resourceName: "star_full") : #imageLiteral(resourceName: "star") , for: .normal)
			}
			.disposed(by: disposeBag)
	}
	
	func setupEventSaveAllMangaBarButton() {
		saveAllMangaButton.rx.tap
			.bind { [weak self] _ in
				self?.saveAllMangaButton.isUserInteractionEnabled = false
				self?.saveAllManga()
			}
			.disposed(by: disposeBag)
	}
	
	func saveAllManga() {
		let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
		let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
		loadingIndicator.hidesWhenStopped = true
		loadingIndicator.style = UIActivityIndicatorView.Style.medium
		loadingIndicator.startAnimating();
		alert.view.addSubview(loadingIndicator)
		present(alert, animated: true, completion: nil)
		saveMangeFormChapterAll(alert: alert)
	}
	
	private func saveMangeFormChapterAll(alert: UIAlertController) {
		var counting = 0
		let fullCount = reloadMangeChapter.value.count
		var buildImageResp = [MapMangeImageResponseModel]()
		let id = self.id
		for capter in reloadMangeChapter.value {
			ManageImageService()
				.callService(request: ManageImageRequestModel(id: id, chapter: capter.part, nogzip: "1"))
			.map { (url) -> [MapMangeImageResponseModel] in return url.result ?? [] }
			.observeOn(MainScheduler.asyncInstance)
			.bind(onNext: { (resp) in
				buildImageResp.append(contentsOf: resp)
				alert.title = "กำลังเตรียม"
				alert.message = "\(counting) | \(fullCount) -> (\(buildImageResp.count))"
				counting += 1
				if counting == fullCount {
					let build = buildImageResp.map { e in e.img }
					Fetch().loadImages(urlStrings: build, alertController: alert, usingBe: build.chunked(into: 100), maxLoad: 100)
				}
			})
			.disposed(by: disposeBag)
		}
	}
		
	private func setFavoriteToFirebase(with models: [MangeFavoriteModel]) {
		guard let userId = try? UserStoreInApp.getUserLogin() else {
			return
		}
		FirebaseStore.setFavoriteToFirebase(with: userId, and: models)
	}
	
	func setupAnimationSeeToRowHistory(animated: Bool) {
		guard let indexPath = didSelectionTableViewHistoryRow else {
			return
		}
		tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.middle, animated: animated)
		
		do {
			didSelectionTableViewHistoryRow = nil
		}
	}
}
