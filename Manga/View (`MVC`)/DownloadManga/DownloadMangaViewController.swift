import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class DownloadMangaViewController: BaseViewController {
	
	@IBOutlet weak var tableView: UITableView!
	
	var reloadMangeChapter: BehaviorRelay<[MapMangePathResponseModel]> = BehaviorRelay<[MapMangePathResponseModel]>(value: [])
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.register(UINib(nibName: "DownloadMangaTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "DownloadMangaTableViewCell")
		
		let dataSource = RxTableViewSectionedAnimatedDataSource<AnimatableSectionModel<Int, MapMangePathResponseModel>>.init(configureCell: { (dataSource, tableView, indexPath, element) -> UITableViewCell in
			let cell = tableView.dequeueReusableCell(withIdentifier: "DownloadMangaTableViewCell", for: indexPath) as! DownloadMangaTableViewCell
			cell.titleLabel?.text = "Chapter"
			cell.detailLabel?.text = element.name
			
			cell.downloadButton.rx.tap
				.bind(onNext: { _ in
					
				})
				.disposed(by: cell.disposeBag)
			return cell
		})
		
		reloadMangeChapter
			.map({ (result) -> [AnimatableSectionModel<Int, MapMangePathResponseModel>] in
				return [AnimatableSectionModel<Int, MapMangePathResponseModel>.init(model: 0, items: result)]
			})
			.bind(to: tableView.rx.items(dataSource: dataSource))
			.disposed(by: disposeBag)
	}
}
