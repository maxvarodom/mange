import UIKit
import AVKit
import SwiftSoup

struct ChapterAnimeXMLModel {
	let title: String
	let chapterLink: String
	let isRead: Bool
}

class ChapterAnimeViewController: BaseViewController {
	
	@IBOutlet weak var tableView: UITableView!
	var addFavoriteButton: UIButton!
	
	var prepareStoryAnime: StoryAnimeXMLModel?
	var chapters: [ChapterAnimeXMLModel]? = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		title = prepareStoryAnime?.titleStory
		tableView.dataSource = self
		tableView.delegate = self
		setupNavigationBarAddFavoriteButton()
		setupEventAddFavoriteBarButton()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		requestAnimeSugoiChpater()
	}
	
	private func requestAnimeSugoiChpater() {
		
		guard let storyLink = prepareStoryAnime?.urlStory else {
			navigationController?.popViewController(animated: true)
			return
		}
		
		let request = NSMutableURLRequest(url: NSURL(string: storyLink)! as URL,
										  cachePolicy: .useProtocolCachePolicy,
										  timeoutInterval: 10.0)
		request.httpMethod = "GET"
		let session = URLSession.shared
		let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
			if !(error != nil) {
				self?.htmlToJSON(data: data!)
				DispatchQueue.main.async {
					self?.tableView.reloadData()
				}
			}
		})
		
		dataTask.resume()
	}
	
	private func htmlToJSON(data: Data) {
		if let html = String(data: data, encoding: .utf8) {
			do {
				chapters?.removeAll()
				let els: Elements = try SwiftSoup.parse(html).select("div").select("center").select("div").select("a")
				for (link) in els.array() {
					let linkHref: String = try link.attr("href")
					let linkText = try link.html()
					let isRead = Store.getSaveHistory().contains(where: { $0.id == linkHref && $0.capter ==  linkText && $0.read ?? false})
					chapters?.append(ChapterAnimeXMLModel(title: linkText, chapterLink: linkHref, isRead: isRead))
				}
			} catch {
				handleAlertViews(error: error)
			}
		}
	}
}

extension ChapterAnimeViewController {
	
	func setupNavigationBarAddFavoriteButton() {
		addFavoriteButton = UIButton(type: .custom)
		addFavoriteButton.setImage((prepareStoryAnime?.favorite ?? false) ? #imageLiteral(resourceName: "star_full") : #imageLiteral(resourceName: "star") , for: .normal)
		addFavoriteButton.frame = CGRect(x: 0, y: 0, width: 31, height: 31)
		let barButton = UIBarButtonItem(customView: addFavoriteButton)
		barButton.width = 31
		self.navigationItem.rightBarButtonItems = [barButton]
	}
	
	func setupEventAddFavoriteBarButton() {
		addFavoriteButton.rx.tap
			.bind { [weak self] (_) in
				if (self?.prepareStoryAnime?.favorite ?? false) {
					guard let id = self?.prepareStoryAnime?.urlStory else { return }
					let retureValueFavoriteInfo = Store.removeFavorite(with: id)
					self?.setFavoriteToFirebase(with: retureValueFavoriteInfo)
					self?.prepareStoryAnime?.favorite = false
				} else {
					guard let id = self?.prepareStoryAnime?.urlStory else { return }
					let createMangeFavorite = MangeFavoriteModel(id: id, favorite: true)
					let retureValueFavoriteInfo = Store.setFavorite(with: createMangeFavorite)
					self?.setFavoriteToFirebase(with: retureValueFavoriteInfo)
					self?.prepareStoryAnime?.favorite = true
				}
				let isFavorite = (self?.prepareStoryAnime?.favorite ?? false)
				self?.addFavoriteButton.setImage(isFavorite ? #imageLiteral(resourceName: "star_full") : #imageLiteral(resourceName: "star") , for: .normal)
			}
			.disposed(by: disposeBag)
	}

	private func setFavoriteToFirebase(with models: [MangeFavoriteModel]) {
		guard let userId = try? UserStoreInApp.getUserLogin() else {
			return
		}
		FirebaseStore.setFavoriteToFirebase(with: userId, and: models)
	}
}

extension ChapterAnimeViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return chapters?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ChapterAnimeTableViewCell", for: indexPath)
		guard let item = chapters?[indexPath.row] else { return cell }
		cell.textLabel?.text = item.title
		cell.detailTextLabel?.text = item.chapterLink
		cell.textLabel?.numberOfLines = 0
		cell.detailTextLabel?.numberOfLines = 0
		cell.backgroundColor = item.isRead ?  UIColor.gray : UIColor.white
		return cell
	}
}

extension ChapterAnimeViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		router?.pushToAnimePlayerViewController(chapter: chapters?[indexPath.row])
	}
}
