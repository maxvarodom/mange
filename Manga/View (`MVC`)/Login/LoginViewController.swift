import UIKit
import RxCocoa
import RxSwift

class LoginViewController: BaseViewController {
	
	@IBOutlet weak var usernameTextField: UITextField!
			
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(true, animated: true)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	@IBAction func loginButton(sender: UIButton) {
		UserStoreInApp.setUserLogin(with: usernameTextField.text!)
		router?.pushToMangaViewController()
	}
}
