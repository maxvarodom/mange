import UIKit
import RxSwift
import RxCocoa

class SplashScreenViewController: BaseViewController {
	
	@IBOutlet weak var statusLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupValidateUserEvent()
		statusLabel.text = "กำลังเข้าสู่ระบบ"
		Store.read()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(true, animated: true)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	func setupValidateUserEvent() {
				
		guard let userId = try? UserStoreInApp.getUserLogin() else {
			router?.pushToLoginViewController()
			return
		}
		
		let firebaseFavorites = FirebaseStore.getFavoritesFromFirebase(firebaseChild: userId)
		let firebaseHistory = FirebaseStore.getSaveHistoryFromFirebase(firebaseChild: userId)
		
		Observable
			.combineLatest(firebaseFavorites, firebaseHistory)
			.do(onNext: { [weak self] (_) in
				self?.statusLabel.text = "เชื่อมต่อ Firebase สำเร็จ"
			})
			.subscribe(onNext: { [weak self] (favorites, history) in
				Store.replaceFavoriteToUserDefaults(with: favorites)
				Store.replaceHistoryToUserDefaults(with: history)
				self?.router?.pushToMangaViewController()
				}, onError: { [weak self] (error) in
					self?.router?.pushToLoginViewController()
			})
			.disposed(by: disposeBag)
	}
}
