import UIKit

class PrepraViewController {
	
	weak var viewController: UIViewController?
	
	init(viewController: UIViewController) {
		self.viewController = viewController
	}
	
	func pushToLoginViewController() {
		let storyboard = UIStoryboard(name: "Login", bundle: nil)
		if let createViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(createViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(createViewController, animated: true)
		}
	}
	
	func pushToHomeViewController() {
		let storyboard = UIStoryboard(name: "Home", bundle: nil)
		if let createViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(createViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(createViewController, animated: true)
		}
	}
	
	func pushToAnimeViewController() {
		let storyboard = UIStoryboard(name: "Anime", bundle: nil)
		if let createViewController = storyboard.instantiateViewController(withIdentifier: "AnimeViewController") as? AnimeViewController {
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(createViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(createViewController, animated: true)
		}
	}
	
	func pushToChapterAnimeViewController(storyAnime: StoryAnimeXMLModel?) {
		let storyboard = UIStoryboard(name: "ChapterAnime", bundle: nil)
		if let createViewController = storyboard.instantiateViewController(withIdentifier: "ChapterAnimeViewController") as? ChapterAnimeViewController {
			createViewController.prepareStoryAnime = storyAnime
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(createViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(createViewController, animated: true)
		}
	}
	
	func pushToAnimePlayerViewController(chapter: ChapterAnimeXMLModel?) {
		let storyboard = UIStoryboard(name: "AnimePlayer", bundle: nil)
		if let createViewController = storyboard.instantiateViewController(withIdentifier: "AnimePlayerViewController") as? AnimePlayerViewController {
			createViewController.prepareChapter = chapter
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(createViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(createViewController, animated: true)
		}
	}
	
	func pushToMangaViewController() {
		let storyboard = UIStoryboard(name: "Manga", bundle: nil)
		if let createViewController = storyboard.instantiateViewController(withIdentifier: "MangaViewController") as? MangaViewController {
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(createViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(createViewController, animated: true)
		}
	}
	
	func pushChapterViewController(with id: String?) {
		let storyboard = UIStoryboard(name: "Chapter", bundle: nil)
		if let createViewController = storyboard.instantiateViewController(withIdentifier: "ChapterViewController") as? ChapterViewController {
			createViewController.id = id
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(createViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(createViewController, animated: true)
		}
	}
	
	func pushReadMangeViewController(with readManageModel: ReadMangeModel, allChapter managePathResponseModel: ManagePathResponseModel?) {
		let storyboard = UIStoryboard(name: "ReadMange", bundle: nil)
		if let createViewController = storyboard.instantiateViewController(withIdentifier: "ReadMangeViewController") as? ReadMangeViewController {
			createViewController.readMangeModel = readManageModel
			createViewController.prepareMangeAllChapter = managePathResponseModel
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(createViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(createViewController, animated: true)
		}
	}
}

