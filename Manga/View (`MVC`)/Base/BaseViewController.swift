import UIKit
import RxSwift
import RxCocoa

class BaseViewController: UIViewController {
	
	private lazy var loadingBlockScreenView = LoadingBlockScreen(frame: UIScreen.main.bounds)
	var router: PrepraViewController?
	var disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.router = PrepraViewController(viewController: self)
	}
}

extension BaseViewController {
	
	func handleAlertViews(error: Error) {
		DispatchQueue.main.async {
			guard let errorCase = error as? ServiceError else {
				self.alertView(title: "ขออภัยค่ะ", message: "\(error)", preferredStyle: .alert)
				return
			}
			switch errorCase {
			case .noNextChapter:
				self.alertView(title: "ไม่มีตอนถัดไปครัชช!", preferredStyle: .alert)
			default:
				self.alertView(title: "ขออภัยค่ะ", message: "\(error)", preferredStyle: .alert)
			}
		}
	}
	
	private func alertView(title: String? = nil, message: String? = nil, preferredStyle: UIAlertController.Style) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		self.present(alert, animated: true)
	}
}

extension BaseViewController {
	
	func showLoading(message: String) {
		DispatchQueue.main.async {
			UIApplication.shared.keyWindow?.addSubview(self.loadingBlockScreenView)
			self.loadingBlockScreenView.text?.text = message
			self.loadingBlockScreenView.show()
		}
	}
	
	func hideLoading() {
		DispatchQueue.main.async {
			self.loadingBlockScreenView.hide()
		}
	}
}
