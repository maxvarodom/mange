import UIKit

class LoadingBlockScreen: UIView {
	
	private var darkVw: UIView?
	private var progressVw: UIActivityIndicatorView?
	var bluerSize = CGSize(width: 60, height: 60)
	var text: UILabel?
	var isProgressBoxShow = false
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setUp()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setUp() {
		darkVw = UIView.init(frame: self.frame)
		darkVw?.backgroundColor = UIColor.black
		darkVw?.alpha = 0
		guard let darkVw = self.darkVw else { return }
		self.addSubview(darkVw)
		
		let blurEffect = UIBlurEffect.init(style: .dark)
		let blurVw = UIVisualEffectView.init(effect: blurEffect)
		blurVw.frame = CGRect(origin: CGPoint.zero, size: bluerSize)
		blurVw.center = self.center
		blurVw.layer.cornerRadius = 10
		blurVw.clipsToBounds = true
		
		text = UILabel()
		text?.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
		text?.center = CGPoint(x: self.center.x, y: self.center.y + 30)
		text?.textAlignment = .center
		text?.font = UIFont(name: "Prompt-Regular", size: 16.0)
		text?.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
		guard let text = text else { return }
		
		let loadingVwFrame = CGRect(origin: CGPoint.zero, size: CGSize(width: 30, height: 30))
		progressVw = UIActivityIndicatorView(style: .white)
		progressVw?.frame = loadingVwFrame
		progressVw?.center = self.center
		guard let progressVw = progressVw else { return }
		
		guard isProgressBoxShow else {
			self.addSubview(progressVw)
			self.addSubview(text)
			return
		}
		self.addSubview(blurVw)
		self.addSubview(progressVw)
	}
	
	func show() {
		self.progressVw?.alpha = 1.0
		self.progressVw?.startAnimating()
		UIView.animate(withDuration: 0.25) {
			self.darkVw?.alpha = 0.7
		}
	}
	
	func hide() {
		UIView.animate(withDuration: 0.5, animations: {
			self.progressVw?.alpha = 0
			self.darkVw?.alpha = 0.0
		}) { (Bool) in
			self.progressVw?.stopAnimating()
			self.removeFromSuperview()
		}
	}
	
}
