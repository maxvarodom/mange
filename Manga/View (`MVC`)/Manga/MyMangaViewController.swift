import UIKit
import RxCocoa
import RxSwift

class MyMangaViewController: BaseViewController {
	
	@IBOutlet weak var tableView: UITableView!

	var favorites: [MangeFavoriteModel] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.dataSource = self
		tableView.delegate = self
		
		favorites = Store.getFavorite()
		callDetailService()
	}
	
	func callDetailService() {
		for value in Store.getFavorite() {
			ManageChapterService().callService(request: ManagePathRequestModel(id: value.id, nogzip: "1"))
				.subscribeOn(MainScheduler.asyncInstance)
				.observeOn(MainScheduler.asyncInstance)
				.subscribe(onNext: { [weak self] (response) in
					guard let `self` = self else { return }
					if let index = self.favorites.firstIndex(where: { (aE) -> Bool in
						return aE.id == response.desc?.id
					}) {
						self.favorites[index].desc = response.desc
						self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
					}
					}, onError: { (error) in
						
				})
				.disposed(by: disposeBag)
		}
	}
}

extension MyMangaViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return favorites.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "MyMangaTableViewCell", for: indexPath) as! MyMangaTableViewCell
		let desc = favorites[indexPath.row].desc
		cell.picImageView.loadImageFromStore(urlString: desc?.img)
		cell.authorLabel.text = desc?.author
		cell.artistLabel.text = desc?.artist
		cell.genresLabel.text = desc?.genres
		cell.statusLabel.text = desc?.status
		cell.detailLabel.text = desc?.detail
		cell.nameLabel.text = desc?.name
		cell.ratingLabel.text = desc?.rating
		return cell
	}
}

extension MyMangaViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let favorite = favorites[indexPath.row]
		router?.pushChapterViewController(with: favorite.id)
	}
}

class MyMangaTableViewCell: UITableViewCell {
	
	@IBOutlet weak var picImageView: UIImageView!
	@IBOutlet weak var authorLabel: UILabel!
	@IBOutlet weak var artistLabel: UILabel!
	@IBOutlet weak var genresLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var ratingLabel: UILabel!
}
