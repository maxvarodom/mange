import UIKit
import RxCocoa
import RxSwift
import AlamofireImage
import RxDataSources
import Differentiator
import ObjectMapper
import MessageUI
import PullToRefreshKit

enum ModeLock {
	case scroll
	case reload
}

class MangaViewController: BaseViewController {
	
	@IBOutlet weak var collectionView: UICollectionView!
	
	var manges: [MapManageSeedResponseModel] = []
	
	var refreshControl: UIRefreshControl?
	var isScrollDownNextPage = false
	var isFav = false
	var isLockRequest = false
	var page = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		collectionView.dataSource = self
		collectionView.delegate = self
		collectionView.keyboardDismissMode = .onDrag
		setupRefreshControlTableView()
		let settingMenu = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(handleSettingMenu(sender:)))
		self.navigationItem.title = "มังงะ งาาาาาาา เงี้ยววว"
		self.navigationItem.rightBarButtonItem = settingMenu
		self.navigationItem.hidesBackButton = true
		self.navigationItem.hidesSearchBarWhenScrolling = false
		let viewController = storyboard?.instantiateViewController(withIdentifier: "SearchMangaViewController") as! SearchMangaViewController
		viewController.viewController = self
		self.navigationItem.searchController = UISearchController(searchResultsController: viewController)
		self.navigationItem.searchController?.delegate = viewController
		self.navigationItem.searchController?.searchResultsUpdater = viewController
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(false, animated: true)
		
		if ChapterViewController.isFavReload {
			ChapterViewController.isFavReload = false
			if let index = manges.firstIndex(where: { (aE) -> Bool in return aE.id == ChapterViewController.id }) {
				self.manges[index].favorite = ChapterViewController.favorite
				collectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
			}
		}
	}

	@objc func handleSettingMenu(sender: UIBarButtonItem) {
		let alertController = UIAlertController(title: "ตั้งค่า", message: "มีอะไรสอบถามมาได้ที่ email: varodom.naul@gmail.com", preferredStyle: .actionSheet)
		alertController.addAction(UIAlertAction(title: isFav ? "ไม่โชว์โชว์มังงะที่ชอบ" : "โชว์มังงะที่ชอบ" , style: .default, handler: { [weak self] _ in
			self?.isFav.toggle()
			self?.requestManage(mode: .reload)
		}))
		
		alertController.addAction(UIAlertAction(title: "เคียรูป", style: .default, handler: { [weak self] _ in
			let alert = UIAlertController(title: "กำลังเริ่มเคียรูป", message: "Please wait...", preferredStyle: .alert)
			let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
			loadingIndicator.hidesWhenStopped = true
			loadingIndicator.style = UIActivityIndicatorView.Style.medium
			loadingIndicator.startAnimating();
			alert.view.addSubview(loadingIndicator)
			self?.present(alert, animated: true, completion: nil)
			SaveImageToFile().removeAll(callback: { counting, fullCount, error in
				alert.title = "เคียยยยยย!!!!!!!"
				alert.message = "\(counting) | \(fullCount)"
				
				if error != nil {
					alert.dismiss(animated: true)
					return
				}
				
				if counting == fullCount {
					alert.dismiss(animated: true)
					return
				}
			})
		}))
		
		alertController.addAction(UIAlertAction(title: "เปิดหน้ามังงะของฉัน", style: .default, handler: { [weak self] _ in
			let viewController = self?.storyboard?.instantiateViewController(withIdentifier: "MyMangaViewController") as! MyMangaViewController
			self?.navigationController?.pushViewController(viewController, animated: true)
		}))
		
		alertController.addAction(UIAlertAction(title: "ส่งหาผู้พัฒนา", style: .default, handler: { [weak self] _ in
			if MFMailComposeViewController.canSendMail() {
				let mail = MFMailComposeViewController()
				mail.mailComposeDelegate = self
				mail.setToRecipients(["varodom.naul@gmail.com"])
				mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
				self?.present(mail, animated: true)
			}
		}))
		
		alertController.addAction(UIAlertAction(title: "ปิด", style: .cancel, handler: { _ in
			alertController.dismiss(animated: true)
		}))
		
		if UIDevice.current.userInterfaceIdiom == .pad {
			if let popoverController = alertController.popoverPresentationController {
			   popoverController.barButtonItem = sender
			 }
			
			self.present(alertController, animated: true, completion: nil)
			
			return
		}
		
		

		present(alertController, animated: true)
	}
	
	func setupRefreshControlTableView() {
		self.collectionView.switchRefreshHeader(to: .refreshing)
		self.requestManage(mode: .reload)
		collectionView.configRefreshHeader(container: self) {
			self.requestManage(mode: .reload)
		}
		
		collectionView.configRefreshFooter(container: self) {
			self.requestManage(mode: .scroll)
		}
	}
		
	func requestManage(mode: ModeLock) {
			
		if mode == .reload {
			self.page = 0
			self.manges.removeAll()
		}
		
		self.refreshControl?.beginRefreshing()
		MangaSeedService()
			.callService(request: ManageSeedRequestModel(nogzip: "1", page: "\(page)"))
			.subscribeOn(globalScheduler)
			.observeOn(globalScheduler)
			.filterNilManageSeedArray()
			.updateStoreFromFireBase()
			.insertFavoriteToModel()
			.observeOn(MainScheduler.asyncInstance)
			.subscribe(onNext: { [weak self] (result) in
				self?.page += 1
				self?.isLockRequest = true
				self?.isScrollDownNextPage = true
				let newResult = result.filter({ (r) -> Bool in
					if self?.isFav ?? false {
						return r.favorite
					} else {
						return true
					}
				})
				
				self?.manges.append(contentsOf: newResult)
				self?.collectionView.reloadData()
				self?.collectionView.switchRefreshHeader(to: .normal(RefreshResult.success, 0.25))
				self?.collectionView.switchRefreshFooter(to: .normal)
				
				}, onError: { (error) in
					print(error)
			})
			.disposed(by: disposeBag)
	}
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}

extension MangaViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { manges.count }
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ManageSeedCollectionViewCell", for: indexPath) as! ManageSeedCollectionViewCell
		let mange = manges[indexPath.row]
		cell.loadingImageForPresentFormDiskOrOutsite(mange)
		cell.setFieldPresentCell(mange)
		cell.feedTodayCell(mange)
		return cell
	}
}

extension MangaViewController: UICollectionViewDelegate {
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let mange = manges[indexPath.row]
		router?.pushChapterViewController(with: mange.id)
	}
}

extension MangaViewController: MFMailComposeViewControllerDelegate {
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		controller.dismiss(animated: true)
	}
}

extension ObservableType {
	
	func insertFavoriteToModel() -> Observable<[MapManageSeedResponseModel]> {
		return map({ (element) -> [MapManageSeedResponseModel] in
			guard let mapManageSeeds = element as? [MapManageSeedResponseModel] else {
				return []
			}
			return mapManageSeeds.map({ (mapManageSeedResponseModel) -> MapManageSeedResponseModel in
				let createMapManageSeedResponseModel = mapManageSeedResponseModel
				let favorite = Store.getFavorite().contains(where: { $0.id == mapManageSeedResponseModel.id && $0.favorite })
				createMapManageSeedResponseModel.favorite = favorite
				return createMapManageSeedResponseModel
			})
		})
	}
}

extension ObservableType {
	
	func updateStoreFromFireBase() -> Observable<Element> {
		guard let userId = try? UserStoreInApp.getUserLogin() else {
			return Observable.empty()
		}
		
		let firebaseFavorites = FirebaseStore.getFavoritesFromFirebase(firebaseChild: userId)
		let firebaseHistory = FirebaseStore.getSaveHistoryFromFirebase(firebaseChild: userId)
		
		return Observable.combineLatest(firebaseFavorites, firebaseHistory)
			.map({ (favorites, history) in
				Store.replaceFavoriteToUserDefaults(with: favorites)
				Store.replaceHistoryToUserDefaults(with: history)
				return
			})
			.flatMap { _ in
				return self
			}
			.subscribeOn(globalScheduler)
			.observeOn(globalScheduler)
	}
}

extension ObservableType {
	
	func filterNilManageSeedArray() -> RxSwift.Observable<[MapManageSeedResponseModel]> {
		return compactMap({ (element) -> [MapManageSeedResponseModel]? in
			return (element as? ManageSeedResponseModel)?.result
		})
	}
}
