import UIKit
import RxSwift
import RxCocoa

class SearchMangaViewController: BaseViewController {
	
	@IBOutlet weak var collectionView: UICollectionView!
	
	var viewController: UIViewController?
	var manges: [MapManageSeedResponseModel] = []

	override func viewDidLoad() {
		super.viewDidLoad()
		collectionView.dataSource = self
		collectionView.delegate = self
		collectionView.keyboardDismissMode = .onDrag
	}
}

extension SearchMangaViewController: UISearchControllerDelegate, UISearchResultsUpdating {
	
	func updateSearchResults(for searchController: UISearchController) {
			
		SearchResultMangeSeed()
			.callService(request: SearchResultMangeSeedRequestModel(query: searchController.searchBar.text ?? ""))
			.subscribeOn(globalScheduler)
			.observeOn(globalScheduler)
			.filterNilManageSeedArray()
			.updateStoreFromFireBase()
			.insertFavoriteToModel()
			.observeOn(MainScheduler.asyncInstance)
			.bind { [weak self] (result) in
				self?.manges = result
				self?.collectionView.reloadData()
		}
		.disposed(by: disposeBag)
	}
}


extension SearchMangaViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { manges.count }
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ManageSeedCollectionViewCell", for: indexPath) as! ManageSeedCollectionViewCell
		let mange = manges[indexPath.row]
		cell.loadingImageForPresentFormDiskOrOutsite(mange)
		cell.setFieldPresentCell(mange)
		cell.feedTodayCell(mange)
		return cell
	}
}

extension SearchMangaViewController: UICollectionViewDelegate {
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let mange = manges[indexPath.row]
		PrepraViewController.init(viewController: viewController!).pushChapterViewController(with: mange.id)
	}
}
